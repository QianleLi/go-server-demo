# README
编译环境：zhimage/golang:1.17.8-sonarqube

## BUILD
```shell
go mod tidy
go build -o cicd-demo-server main.go
```

## Unit Test
```shell
cd utils
go test -v
```

## code analysis
```shell
go get github.com/golang/lint
go install github.com/golang/lint

golint utils
```

## build docker image
```shell
docker build -t zhimage/cicd-demo-server:0.1 .   
```
