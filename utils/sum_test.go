package utils

import "testing"

func TestSum(t *testing.T) {
	if ans := GetSum(1, 2); ans != 3 {
		t.Errorf("1 + 2 expected be 3, but %d got", ans)
	}

	if ans := GetSum(5, 6); ans != 11 {
		t.Errorf("5 + 6 expected be 11, but %d got", ans)
	}
}
